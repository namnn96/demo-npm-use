import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroListModule } from '@namnn96/demo-npm';

@NgModule({
  imports: [
    CommonModule,
    HeroListModule
  ]
})
export class LeapHeroListModule { }
