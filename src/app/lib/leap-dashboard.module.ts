import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardModule } from '@namnn96/demo-npm';

@NgModule({
  imports: [
    CommonModule,
    DashboardModule
  ]
})
export class LeapDashboardModule { }
