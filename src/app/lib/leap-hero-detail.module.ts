import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroDetailModule } from '@namnn96/demo-npm';

@NgModule({
  imports: [
    CommonModule,
    HeroDetailModule
  ]
})
export class LeapHeroDetailModule { }
