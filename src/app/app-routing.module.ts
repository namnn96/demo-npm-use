import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  {
    path: 'dashboard',
    loadChildren: 'app/lib/leap-dashboard.module#LeapDashboardModule'
  },
  {
    path: 'detail/:id',
    loadChildren: 'app/lib//leap-hero-detail.module#LeapHeroDetailModule'
  },
  {
    path: 'heroes',
    loadChildren: 'app/lib//leap-hero-list.module#LeapHeroListModule'
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
